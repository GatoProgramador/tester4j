Tester4J is a Scenario based testing tool.

The main purpose of this project is to be able to create unit tests for legacy Java code
as quickly as possible. To test code that never had any tests (or they no longer work) 
and probably wasn't written with concepts like TDD or BDD in mind.

Or maybe you just can't invest too much time writing conventional unit tests.

Unlike other tools, tester4j doesn't produce any Java code. 
All your tests/scenarios are written in one or more JSON files.

How to use:

	Tester a = new Tester();
	a.test(Tester.loadTestFile("com.tester4j.demo.Examples.json"));

Class under test:

	package com.tester4j.demo;
	
	import org.apache.log4j.Logger;
	
	public class Examples {
	
		private Logger logger = Logger.getLogger(Examples.class);
		
		/**
		 * Overloaded methods supported
		 * 
		 * @param a
		 * @param b
		 * @return
		 */
		public Double sum(Double a, Double b){
			if(a==null || b==null){
				throw new IllegalArgumentException("Both parameters required.");
			}
			return a+b;
		}
		
		/**
		 * Overloaded methods supported
		 * 
		 * @param a
		 * @param b
		 * @return
		 */
		public Float sum(Float a, Float b){
			if(a==null || b==null){
				throw new IllegalArgumentException("Both parameters required.");
			}
			return a+b;
		}
		
		/**
		 * To test parameters' types
		 * 
		 * @param value
		 * @param pow
		 * @return
		 */
		public Double pow(Double value, Integer pow){
			if(value==null || pow==null){
				throw new IllegalArgumentException("Both parameters required.");
			}
			
			if(pow<0)
				throw new IllegalArgumentException("Exponent must be a positive number.");
			
			if(pow==0)
				return 1D;
			
			Double out = 1D;
			for(int i=0; i<pow; i++){
				out = value * out;
			}
			return out;
		}
		
		/**
		 * To test strings and null outputs
		 * 
		 * @param name
		 * @return
		 */
		public String sayHello(String name){
			if(name==null)
				return null;
			
			return "Hello "+name+"!";
		}
		
		/**
		 * To test zero parameter methods.
		 * 
		 * @return
		 */
		public String sayHi(){
			return "Hi!";
		}
		
		/**
		 * To test void return type
		 * 
		 * @param message
		 */
		public void logMessage(String message){
			logger.info("Message: " + message);
		}
		
	}


Example JSON file:

	{
		"classes" : [
			{
				"class": "com.tester4j.demo.Examples",
				"methods" : [
					{
						"__comments" : "Verifies the results of a sum of double values.",
						"name" : "sum",
						"scenarios" : [
							{
								"inputs" : [12,14],
								"output" : 26
							},
							{
								"inputs" : [14.78,56.0001],
								"output" : 70.7801
							},
							{
								"inputs" : [0,0],
								"output" : 0
							},
							{
								"inputs" : [null,14],
								"throws" : "java.lang.IllegalArgumentException"
							},
							{
								"inputs" : [14,null],
								"throws" : "java.lang.IllegalArgumentException"
							}
						],
						"inputTypes" : [
							"java.lang.Double","java.lang.Double"
						],
						"outputType" : "java.lang.Double"
					},
					
					{
						"__comments" : "Verifies the results of a sum of float values. Overloaded method.",
						"name" : "sum",
						"scenarios" : [
							{
								"inputs" : [12,14],
								"output" : 26
							},
							{
								"inputs" : [14.78,56.0001],
								"output" : 70.7801
							},
							{
								"inputs" : [0,0],
								"output" : 0
							},
							{
								"inputs" : [null,14],
								"throws" : "java.lang.IllegalArgumentException"
							},
							{
								"inputs" : [14,null],
								"throws" : "java.lang.IllegalArgumentException"
							}
						],
						"inputTypes" : [
							"java.lang.Float","java.lang.Float"
						],
						"outputType" : "java.lang.Float"
					},
					
					{
						"__comments" : "Here we take mixed input types and a double output type.",
						"name" : "pow",
						"scenarios" : [
							{
								"inputs" : [3,2],
								"output" : 9
							},
							{
								"inputs" : [3,3],
								"output" : 27
							},
							{
								"inputs" : [2,2],
								"output" : 4
							},
							{
								"inputs" : [1,0],
								"output" : 1
							},
							{
								"inputs" : [14,-1],
								"throws" : "java.lang.IllegalArgumentException"
							},
							{
								"inputs" : [14,null],
								"throws" : "java.lang.IllegalArgumentException"
							}
						],
						"inputTypes" : [
							"java.lang.Double","java.lang.Integer"
						],
						"outputType" : "java.lang.Double"
					},
					
					{
						"__comments" : "This method receives and returns strings. We expect a null output when the input is null too.",
						"name" : "sayHello",
						"scenarios" : [
							{
								"inputs" : ["Kitty"],
								"output" : "Hello Kitty!"
							},
							{
								"inputs" : [null],
								"output" : null
							}
						],
						"inputTypes" : [
							"java.lang.String"
						],
						"outputType" : "java.lang.String"
					},
					
					{
						"__comments" : "In this case the method has no inputs (empty array) and the return type is a string.",
						"name" : "sayHi",
						"scenarios" : [
							{
								"inputs" : [],
								"output" : "Hi!"
							}
						],
						"inputTypes" : [],
						"outputType" : "java.lang.String"
					},
					
					{
						"__comments" : "In this case the input type is a string and there's no output (void) so we define it as null.",
						"name" : "logMessage",
						"scenarios" : [
							{
								"inputs" : ["This is a message"],
								"output" : null
							}
						],
						"inputTypes" : ["java.lang.String"],
						"outputType" : null
					},
					
					{
						"__comments" : "When the output is an object... ",
						"name" : "getEmployeeById",
						"inputTypes" : ["java.lang.Long"],
						"outputType" : "com.tester4j.demo.Employee",
						"scenarios" : [
							{
								"inputs" : [1234],
								"output" : {
									"__comments" : "We can indicate what value we expect to see in each attribute.",
							   		"name" : "John",
							   		"salary" : 45000,
							   		"active" : true,
							   		"project" : {
							   			"__comments" : "We can also add nested objects and evaluate their attributes.",
							   			"id" : 13,
							   			"active" : true,
							   			"department" : {
							   				"__comments" : "There are no limits when it comes to nesting.",
							   				"id" : 2,
							   				"name" : "Staff"
							   			}
							   		}
							   }
							},
							
							{
								"__comments" : "However we don't have to evaluate everything. Attributes not listed will not be evaluated.",
								"inputs" : [22],
								"output" : {
							   		"name" : "James",
							   		"salary" : 32800.45
							   }
							},
							
							{
								"__comments" : "There's no such employee so we expect to get a null.",
								"inputs" : [0],
								"output" : null
							}
						]
					}
					
					
				]
			}
		]
	}

This project here is only the core, there will be a Maven plugin so you can run your all your tests as part of the testing lifecycle.

Also, I expect to be able to write an Eclipse plug-in so you can create the JSON test file(s) using a GUI.
Part of the JSON code like the types, methods and class names should be inferred by the IDE; leaving the most important thing, the scenarios, to the user.



