package com.tester4j.demo;

import org.apache.log4j.Logger;

public class Examples {

	private Logger logger = Logger.getLogger(Examples.class);
	
	/**
	 * Overloaded methods supported
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public Double sum(Double a, Double b){
		if(a==null || b==null){
			throw new IllegalArgumentException("Both parameters required.");
		}
		return a+b;
	}
	
	/**
	 * Overloaded methods supported
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public Float sum(Float a, Float b){
		if(a==null || b==null){
			throw new IllegalArgumentException("Both parameters required.");
		}
		return a+b;
	}
	
	/**
	 * To test parameters' types
	 * 
	 * @param value
	 * @param pow
	 * @return
	 */
	public Double pow(Double value, Integer pow){
		if(value==null || pow==null){
			throw new IllegalArgumentException("Both parameters required.");
		}
		
		if(pow<0)
			throw new IllegalArgumentException("Exponent must be a positive number.");
		
		if(pow==0)
			return 1D;
		
		Double out = 1D;
		for(int i=0; i<pow; i++){
			out = value * out;
		}
		return out;
	}
	
	/**
	 * To test strings and null outputs
	 * 
	 * @param name
	 * @return
	 */
	public String sayHello(String name){
		if(name==null)
			return null;
		
		return "Hello "+name+"!";
	}
	
	/**
	 * To test zero parameter methods.
	 * 
	 * @return
	 */
	public String sayHi(){
		return "Hi!";
	}
	
	/**
	 * To test void return type
	 * 
	 * @param message
	 */
	public void logMessage(String message){
		logger.info("Message: " + message);
	}
	
	/**
	 * To test custom type outputs.
	 * 
	 * @param id
	 * @return
	 */
	public Employee getEmployeeById(Long id){
		if(id==1234L){
			Employee e = new Employee();
			e.setActive(true);
			e.setName("John");
			e.setSalary(45000.0D);
			e.setProject(new Project(13, "Sourcing", true));
			return e;
		}else if(id==22L){
			Employee e = new Employee();
			e.setActive(true);
			e.setName("James");
			e.setSalary(32800.45D);
			return e;
		}else{
			return null;
		}
	}
	
}
