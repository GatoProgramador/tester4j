package com.tester4j;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Tester {

	static Logger logger = Logger.getLogger(Tester.class);
	
	/**
	 * Temporal method used to test the main class
	 * 
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Tester a = new Tester();
		a.test(Tester.loadTestFile("com.tester4j.demo.Examples.json"));
	}
	
	/**
	 * Reads and parses a JSON file in tester4j notation.
	 * The file's location should be relative to the class path.
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public static JSONObject loadTestFile(final String fileName) throws Exception{
		InputStream fis = null;
		try {
			fis = Tester.class.getClassLoader().getResourceAsStream(fileName);
			return new JSONObject( IOUtils.toString(fis, "UTF-8") );
		} catch (JSONException | IOException e) {
			logger.error("Failed to load test file '"+fileName+"'", e);
			throw e;
		} finally {
			try {
				if(fis!=null)
					fis.close();
			} catch (IOException e) {
				logger.warn("Input stream could not be closed.");
			}
		}
	}

	/**
	 * Evaluates all classes found in the AutoTester JSON configuration file
	 * 
	 * @param json
	 * @return
	 */
	public boolean test(JSONObject json) {
		if(json==null || !json.has("classes")){
			throw new IllegalArgumentException("The provided AutoTester JSON file has no test classes.");
		}
		
		boolean allPass = true;
		JSONArray classes = json.getJSONArray("classes");
		for(int i=0; i<classes.length(); i++){
			TestClass cut = new TestClass((JSONObject)classes.get(i));
			allPass = allPass && evaluate(cut);
		}
		
		if(allPass)
			logger.info("-- All tests passed --");
		else
			logger.info("-- Some tests did not pass --");
		
		return allPass;
	}

	/**
	 * Evaluates all methods found in the provided class
	 * 
	 * @param cut
	 * @param index
	 * @return
	 */
	private boolean evaluate(TestClass cut) {
		if(null == cut.getMethods()){
			logger.warn("There are no methods to test in class '"+cut.getClassName()+"'.");
			return true;
		}
		
		boolean allPass = true;
		for(TestMethod method : cut.getMethods()){
			allPass = allPass && evaluate(method);
		}
		return allPass;
	}

	/**
	 * Evaluates all scenarios found inside a method under test, if any.
	 * 
	 * @param jsonObject
	 * @param i
	 * @return
	 */
	private boolean evaluate(TestMethod mut) {
		if(null == mut.getScenarios() || mut.getScenarios().isEmpty()){
			logger.warn("The method '"+mut.getName()+"' has no test scenarios.");
			return true;
		}
		
		boolean allPass = true;
		for(TestScenario scenario : mut.getScenarios()){
			allPass = allPass && evaluate(scenario);
		}
		return allPass;
	}
	
	/**
	 * Executes a given method with the provided inputs for a specific scenario.
	 *  
	 * @param scenario
	 * @return true when the expected outcome (output, throws) is met, false otherwise
	 */
	@SuppressWarnings("rawtypes")
	private boolean evaluate(TestScenario scenario) {
		final String className = scenario.getParentClass().getClassName();
		final String methodName = scenario.getParentMethod().getName();
		final Class[] inputTypeClasses = scenario.getParentMethod().getInputTypesClasses();
		final List<String> inputTypes = scenario.getParentMethod().getInputTypes();
		final String expectedException = scenario.getThrowsClass();
		final List<Object> inputs = scenario.getInputs();
		
		try {
			Object instance = Class.forName(className).newInstance();
			Method method = getMethod(className, methodName, inputTypeClasses);
			
			Object output;
			try {
				output = method.invoke(instance, inputs==null ? null : inputs.toArray());
			} catch (Throwable e) {
				String thrownException = e.getClass().getName();
				
				// Find the actual exception:
				if(thrownException.equalsIgnoreCase(InvocationTargetException.class.getName())){
					thrownException = e.getCause().getClass().getName();
				}
				
				// If exception matches 'throws' value then pass.
				if(expectedException!=null && thrownException.equals(expectedException)){
					logger.info("Pass: "+methodName+"("+inputs+") throws "+expectedException);
					return true;
				}else{
					throw e;
				}
			}
			
			return evaluateScenario(scenario, output, methodName+"("+inputs+")");
			
		} catch (InstantiationException e) {
			logger.error("Failed to create instance of class '"+className+"'.", e);
		} catch (IllegalAccessException e) {
			logger.error("Failed to create instance of class '"+className+"'.", e);
		} catch (ClassNotFoundException e) {
			logger.error("Class under test has not been found for '"+className+"'.", e);
		} catch (NoSuchMethodException e) {
			logger.error("No such method " + methodName + "("+inputTypes+") for class '"+className+"'.", e);
		} catch (SecurityException e) {
			logger.error("Method "+methodName+"("+inputTypes+") is not accesible. Class '"+className+"'.", e);
		} catch (IllegalArgumentException e) {
			logger.error("Illegal input arguments "+inputTypes+" for method '"+methodName+"'.", e);
		} catch (InvocationTargetException e) {
			logger.error("Cannot invoke method '"+methodName+"("+inputTypes+")'.", e);
		}
		return false;
	}

	/**
	 * Verifies if the actual output matches with the expected output defined 
	 * in the given scenario.
	 * 
	 * @param scenario
	 * @param methodName
	 * @param inputs
	 * @param output
	 * @return
	 */
	private boolean evaluateScenario(TestScenario scenario, Object output,
			final String methodNameAndParams) {
		
		final Object expectedOutput = scenario.getOutput();
		
		if(output == null && expectedOutput==null){
			logger.info("Pass: "+methodNameAndParams);
			return true;
		}
		if(!isAPrimitive(output)){
			return compareObjects(output, scenario);
		}else{
			return comparePrimitives(output, expectedOutput, methodNameAndParams);
		}
	}

	/**
	 * Compares two atomic primitive values.
	 * 
	 * @param output
	 * @param expectedOutput
	 * @param methodNameAndParams - used for logging purposes only
	 * @return
	 */
	private boolean comparePrimitives(Object output, final Object expectedOutput,
			final String methodNameAndParams) {
		if(output != null && output.equals(expectedOutput)){
			logger.info("Pass: "+methodNameAndParams+" returns "+expectedOutput);
			return true;
		}
		else{
			logger.info("Fail: "+methodNameAndParams+", expected: " + expectedOutput+", got: "+output);
			return false;
		}
	}

	/**
	 * First it tries to obtain a public method. If this fails then we try to obtain
	 * a private or protected method.
	 * 
	 * @param className
	 * @param methodName
	 * @param inputTypeClasses
	 * @return
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("rawtypes")
	private Method getMethod(final String className, final String methodName,
			final Class[] inputTypeClasses) throws ClassNotFoundException, NoSuchMethodException {
		Method method;
		try {
			method = Class.forName(className).getMethod(methodName, inputTypeClasses);
		} catch (NoSuchMethodException e) {
			method = Class.forName(className).getDeclaredMethod(methodName, inputTypeClasses);
			method.setAccessible(true);
		}
		return method;
	}

	/**
	 * 
	 * @param output
	 * @return true when the object is null or belongs to the "java.lang" package, false otherwise.
	 */
	private boolean isAPrimitive(Object output) {
		if(output==null || output.getClass().getName().contains("java.lang.")){
			return true;
		}
		return false;
	}

	/**
	 * Compares the output-attribute-values with the expected-attribute-values for this scenario.
	 * The result of this method is not a simple output.equals(scenario.getOutput()), all attributes listed must match one by one.
	 * 
	 * Example:
	 * 
	 * "output" : {
	 * 		"name" : "John",
	 * 		"salary" : 45000,
	 * 		"active" : true
	 * }
	 * 
	 * In this case the output object should have the same 'name', 'salary' and 'active' values in order for the
	 * scenario to pass (true). If one or more of those attributes doesn't match then the scenario fails (false).
	 * 
	 * The same applies when the 'output' lists one or more nested objects:
	 * 
	 * "output" : {
	 * 		"name" : "John",
	 * 		"salary" : 45000,
	 * 		"active" : true,
	 * 		"project" : {
	 * 			"id" : 13,
	 * 			"active" : true,
	 * 			"department" : {
	 * 				"id" : 2,
	 * 				"name" : "Staff"
	 * 			}
	 * 		}
	 * }
	 * 
	 * In this second scenario the output object should have all those values seen in the example above, including the
	 * values seen in the project and department objects. If for some reason the project or the department are null then
	 * the scenario will fail too.
	 * 
	 * There is no limit in the nested objects level of depth. 
	 * There should be getters for all attributes, objects and primitives, that appear in the scenario. 
	 * 
	 * @param output
	 * @param scenario
	 * @return true when all the attributes present in the scenario match with the corresponding output values.
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * 
	 */
	private boolean compareObjects(Object output, TestScenario scenario) {
		return compareObjects(output, scenario.getOutput(), scenario.getOutputJson());
	}

	/**
	 * Iteratively compares 
	 * 
	 * @param output
	 * @param expected
	 * @param rootObject
	 * @return
	 */
	private boolean compareObjects(Object output, Object expected,
			JSONObject rootObject) {
		
		logger.info("Evaluating output of type "+(output!=null?output.getClass().getName():
			(expected!=null?expected.getClass().getName():"null"))+":");
		
		Iterator<String> attributes = rootObject.keys();
		
		boolean allPass = true;
		
		// For each attribute defined at "output" root level do:
		while(attributes.hasNext()){
			String attr = attributes.next();
			if(attr == null || attr.isEmpty()){
				throw new IllegalArgumentException("Unable to compare values. Attribute name is null or empty.");
			}
			
			Object valueFound;
			Object valueExcepted;
			try {
				valueFound = PropertyUtils.getProperty(output, attr);
				valueExcepted = PropertyUtils.getProperty(expected, attr);
			} catch (Exception e) {
				throw new IllegalArgumentException("Unable to compare values. Attribute or getter not found.");
			}
			
			// When one or the other are null. Comparison fails.
			if((valueFound==null && valueExcepted!=null) || (valueFound!=null && valueExcepted==null)){
				logger.info("Fail: "+(valueFound==null?"output":"expected")+" value should not be null.");
				return false;
			}
			
			// Type mismatch. Comparison fails.
			if(output.getClass().equals(valueExcepted.getClass())){
				logger.info("Fail: Found a "+valueFound.getClass().getName()+" value, "
						+ "expected: "+valueExcepted.getClass().getName());
				return false;
			}
			
			// When both are primitives then:
			if(isAPrimitive(valueFound)){
				boolean matches = comparePrimitives(attr, valueFound, valueExcepted);
				allPass = allPass && matches;
			}else{
				JSONObject nestedObject = rootObject.getJSONObject(attr);
				allPass = allPass && compareObjects(valueFound, valueExcepted, nestedObject);
			}
		}
		return allPass;
	}

	/**
	 * Used for comparing numbers of all types, booleans and Strings.
	 * 
	 * @param attributeName
	 * @param valueFound
	 * @param valueExcepted
	 * @return
	 */
	private boolean comparePrimitives(String attributeName, Object valueFound, Object valueExcepted) {
		boolean matches = valueFound.equals(valueExcepted);
		if(matches){
			logger.info("    Pass: Attribute value '"+attributeName+"' equals "+valueExcepted+".");
		}else{
			logger.info("    Fail: Attribute value '"+attributeName+"' is "+valueFound+", expected: "+valueExcepted+".");
		}
		return matches;
	}

}