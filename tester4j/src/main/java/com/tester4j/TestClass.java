package com.tester4j;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class TestClass {

	private String className;
	private List<TestMethod> methods;
	
	private Logger logger = Logger.getLogger(TestClass.class);
	
	public TestClass(JSONObject cut) {
		super();
		init(cut);
	}
	
	/**
	 * Initializes the current instance with the provided JSON node that 
	 * corresponds to a single "class" JSON Object.
	 * 
	 * @param cut
	 */
	private void init(JSONObject cut){
		logger.debug(cut.toString());
		this.methods = new ArrayList<TestMethod>();
		
		if(cut==null || !cut.has("class")){
			throw new IllegalArgumentException("No class attribute found in class entry.");
		}
		
		this.className = cut.getString("class");
		if(cut==null || !cut.has("methods")){
			logger.warn("The class '"+this.className+"' has no methods to be tested.");
		}
		
		JSONArray methods = cut.getJSONArray("methods");
		for(int i=0; i<methods.length(); i++){
			this.methods.add(new TestMethod((JSONObject)methods.get(i), this));
		}
	}
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public List<TestMethod> getMethods() {
		return methods;
	}
	public void setMethods(List<TestMethod> methods) {
		this.methods = methods;
	}
}
