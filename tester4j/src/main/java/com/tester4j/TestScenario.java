package com.tester4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

public class TestScenario {

	private List<Object> inputs;
	private Object output;
	private JSONObject outputJson;
	private String throwsClass;
	
	private TestMethod parentMethod;
	private TestClass parentClass;
	
	private Logger logger = Logger.getLogger(TestScenario.class);
	
	/**
	 * Initializes the current scenario instance with the data provided in
	 * the JSON Object.
	 * 
	 * @param jsonObject
	 */
	public TestScenario(JSONObject scenario, 
			TestMethod parentMethod, 
			TestClass parentClass) {
		super();
		this.parentClass = parentClass;
		this.parentMethod = parentMethod;
		init(scenario);
	}
	
	/**
	 * Initializes the current scenario instance with the data provided in
	 * the JSON Object.
	 * 
	 * @param jsonObject
	 */
	@SuppressWarnings({ "rawtypes" })
	private void init(JSONObject scenario) {
		if(null == scenario){
			throw new IllegalArgumentException("Scenario cannot be null.");
		}
		
		if(scenario.keySet() == null || scenario.keySet().isEmpty()){
			throw new IllegalArgumentException("Scenario cannot be empty.");
		}
		
		if(scenario.has("inputs")){
			this.inputs = new ArrayList<Object>();
			
			if(!scenario.isNull("inputs")){
				JSONArray inputs = scenario.getJSONArray("inputs");
				
				for(int i=0; i<inputs.length(); i++){
					Class inputClass = this.getParentMethod().getInputTypesClasses()[i];
					this.inputs.add(readValue(inputs, i, inputClass));
				}
			}
		}
		
		/* To be able to reuse the readValue method I convert the single output
		 * into an array and then read the first (only) element.
		 */
		if(scenario.has("output") && !scenario.isNull("output")){
			Class outputClass = this.getParentMethod().getOutputTypeClass();
			JSONArray outputs = new JSONArray();
			outputs.put(scenario.get("output"));
			try{
				this.outputJson = scenario.getJSONObject("output");
			}catch(Exception e){
				logger.debug("Cannot read outputJson, it's not a JSONObject. Primitive assumed.");
			}
			this.output = readValue(outputs, 0, outputClass);
		}
		
		if(scenario.has("throws") && !scenario.isNull("throws")){
			this.throwsClass = scenario.getString("throws");
		}
	}
	
	/**
	 * Converts a single value/object into an instance of the given class.
	 * 
	 * @param inputs
	 * @param i
	 * @param className
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object readValue(JSONArray inputs, int i, Class className){
		
		// Cover primitives first:
		if(inputs.isNull(i)){
			return className.cast(null);
		}
		else if(String.class.equals(className)){
			return inputs.getString(i);
		}
		else if(Integer.class.equals(className)){
			return inputs.getInt(i);
		}
		else if(Long.class.equals(className)){
			return inputs.getLong(i);
		}
		else if(Double.class.equals(className)){
			return inputs.getDouble(i);
		}
		else if(Float.class.equals(className)){
			return new Float(inputs.getDouble(i));
		}
		else if(BigDecimal.class.equals(className)){
			return inputs.getBigDecimal(i);
		}
		else if(Boolean.class.equals(className)){
			return inputs.getBoolean(i);
		}
		// Custom objects of any type:
		else{
			Object rawValue = inputs.get(i);
			if(rawValue instanceof Number){
				return (Number)rawValue;
			}else if(inputs.get(i) instanceof String){
				return (String)rawValue;
			}else if(inputs.get(i) instanceof Boolean){
				return (Boolean)rawValue;
			}
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				JSONObject jsonObject = inputs.getJSONObject(i);
				return mapper.readValue(jsonObject.toString(), className);
			} catch (Exception e) {
				logger.error("Failed to convert input object to class '"+className+"'", e);
				return null;
			}
		}
	}

	public List<Object> getInputs() {
		return inputs;
	}
	public void setInputs(List<Object> inputs) {
		this.inputs = inputs;
	}
	public Object getOutput() {
		return output;
	}
	public void setOutput(Object output) {
		this.output = output;
	}
	public String getThrowsClass() {
		return throwsClass;
	}
	public void setThrowsClass(String throwsClass) {
		this.throwsClass = throwsClass;
	}

	public TestMethod getParentMethod() {
		return parentMethod;
	}

	public TestClass getParentClass() {
		return parentClass;
	}

	public JSONObject getOutputJson() {
		return outputJson;
	}
}