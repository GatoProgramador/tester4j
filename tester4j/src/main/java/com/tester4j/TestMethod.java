package com.tester4j;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class TestMethod {

	private String name;
	private List<TestScenario> scenarios;
	private List<String> inputTypes;
	private String outputType;
	private TestClass parentClass;
	
	@SuppressWarnings("rawtypes")
	private Class[] inputTypesClasses;
	@SuppressWarnings("rawtypes")
	private Class outputTypeClass;
	
	private Logger logger = Logger.getLogger(TestMethod.class);
	
	/**
	 * Fills the current instance with the scenarios found in a single
	 * JSON Object that corresponds to an entry inside the "methods" definition.
	 * 
	 * @param mut
	 */
	public TestMethod(JSONObject mut, TestClass parentClass) {
		super();
		this.parentClass = parentClass;
		init(mut);
	}
	
	/**
	 * Fills the current instance with the scenarios found in a single
	 * JSON Object that corresponds to an entry inside the "methods" definition.
	 * 
	 * @param mut
	 */
	private void init(JSONObject mut) {
		this.inputTypes = new ArrayList<>();
		this.scenarios = new ArrayList<>();
		
		if(mut.isNull("outputType")){
			this.outputType = null;
		}else{
			this.outputType = mut.getString("outputType");
		}
		
		if(mut==null || !mut.has("name")){
			throw new IllegalArgumentException("Method has no qualified name.");
		}
		
		this.name = mut.getString("name");
		if(mut==null || !mut.has("scenarios")){
			logger.warn("The method '"+this.name+"' has no test scenarios.");
		}
		
		JSONArray inputTypes = mut.getJSONArray("inputTypes");
		for(int i=0; i<inputTypes.length(); i++){
			this.inputTypes.add(inputTypes.getString(i));
		}
		
		JSONArray scenarios = mut.getJSONArray("scenarios");
		for(int i=0; i<scenarios.length(); i++){
			this.scenarios.add(new TestScenario((JSONObject)scenarios.get(i), this, this.parentClass));
		}
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<TestScenario> getScenarios() {
		return scenarios;
	}
	public void setScenarios(List<TestScenario> scenarios) {
		this.scenarios = scenarios;
	}

	public TestClass getParentClass() {
		return parentClass;
	}
	
	public List<String> getInputTypes() {
		return inputTypes;
	}

	private void initInputTypesClasses() {
		if(this.inputTypes == null || this.inputTypes.isEmpty())
			this.inputTypesClasses = null; // same as empty array
		
		this.inputTypesClasses = new Class[this.inputTypes.size()];
		
		try {
			for(int i=0; i<this.inputTypes.size(); i++){
				inputTypesClasses[i] = Class.forName(this.inputTypes.get(i));
			}
		} catch (ClassNotFoundException e) {
			logger.fatal("Cannot find class for inputType ", e);
		}
	}

	/**
	 * Converts a list of class names (strings) into an array of java classes.
	 * @return
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	public Class[] getInputTypesClasses(){
		if(this.inputTypes!=null && this.inputTypesClasses==null)
			initInputTypesClasses();
		
		return this.inputTypesClasses;
	}

	public String getOutputType() {
		return outputType;
	}

	/**
	 * Converts the given output type into a java class.
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Class getOutputTypeClass() {
		try {
			if(this.outputType!=null && this.outputTypeClass == null)
				this.outputTypeClass = Class.forName(outputType);
		} catch (ClassNotFoundException e) {
			logger.fatal("Cannot find class for outputType ", e);
		}
			
		return outputTypeClass;
	}
	
}
