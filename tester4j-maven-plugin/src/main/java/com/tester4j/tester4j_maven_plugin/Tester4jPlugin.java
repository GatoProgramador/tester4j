package com.tester4j.tester4j_maven_plugin;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.tester4j.Tester;

@Mojo(name = "runtests")
public class Tester4jPlugin extends AbstractMojo {

	static final String TSTR = "Caught Exception while in getResource(). This may be innocuous.";

	/**
	 * This plug-in will: - Read the default tester4j.xml file from the
	 * project's class-path - Load each JSON test file and run it - Log testing
	 * results - throw a MojoFailureException when one or more tests fail -
	 * throw a MojoExecutionException when an unexpected error occurs
	 * 
	 */
	public void execute() throws MojoExecutionException {
		try {
			getLog().info("=== Executing Tester4j ===");
			getLog().info("Loading configuration file");
			Document doc = loadFile("tester4j.xml");

			if (doc.hasChildNodes()) {
				Tester tester = new Tester();
				NodeList files = doc.getDocumentElement().getElementsByTagName("file");
				boolean allPass = true;

				for (int i = 0; i < files.getLength(); i++) {
					final String testFileName = files.item(i).getTextContent();

					try {
						getLog().info("Running test file: " + testFileName);
						URL testFileURL = getResource(testFileName);
						allPass = allPass && tester.test(loadJSON(testFileURL));
					} catch (Exception e) {
						getLog().error("An error occurred running test file: " 
								+ testFileName, e);
						throw e;
					}
				}

				if (allPass) {
					getLog().info("=== All tests passed ===");
				} else {
					getLog().info("=== Tests failed ===");
					throw new MojoFailureException(
							"Some tests failed. Please see the log for details.");
				}
			} else {
				getLog().warn("No test files found in the tester4j configuration file. Skipping.");
			}

		} catch (Exception e) {
			throw new MojoExecutionException("Failed to read tester4j configuration file", e);
		}

	}

	/**
	 * Opens and parses a JSON test file
	 * 
	 * @param testFileURL
	 * @return
	 * @throws Exception
	 */
	private JSONObject loadJSON(URL testFileURL) throws Exception{
		if(testFileURL == null){
			getLog().error("Cannot read test file with null URL.");
			throw new NullPointerException();
		}
		
		InputStream is = null;
		try {
			is = testFileURL.openConnection().getInputStream();
			return new JSONObject( IOUtils.toString(is, "UTF-8") );
		} catch (JSONException e) {
			getLog().error("Test file '"+testFileURL+"' could not be parsed as JSON", e);
			throw e;
		} catch (IOException e) {
			getLog().error("Could not find test file "+testFileURL);
			throw e;
		} catch(Exception e){
			getLog().error("Could not read test file "+testFileURL);
			throw e;
		} finally {
			if(is!=null){
				is.close();
			}
		}
	}

	/**
	 * Finds the tester4j.xml configuration file from the user's project.
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public Document loadFile(final String fileName) throws Exception {
		InputStream fis = null;
		URLConnection conn = null;
		try {
			URL url = getResource(fileName); 
			conn = url.openConnection();
			fis = conn.getInputStream();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fis);
			return doc;
		} catch (Exception e) {
			getLog().error("Failed to load tester4j configuration file '" + fileName + "'", e);
			throw e;
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				getLog().warn("Input stream could not be closed.");
			}
		}
	}

	private URL getResource(String resource) {
		resource = cleanup(resource);
		
		ClassLoader classLoader = null;
		URL url = null;

		boolean java1 = true;
		boolean ignoreTCL = false;

		try {
			if (!java1 && !ignoreTCL) {
				classLoader = getTCL();
				if (classLoader != null) {
					getLog().debug("Trying to find [" + resource + "] using context classloader "
							+ classLoader + ".");
					url = classLoader.getResource(resource);
					if (url != null) {
						return url;
					}
				}
			}

			// We could not find resource. Let us now try with the classloader 
			// that loaded this class.
			classLoader = Tester4jPlugin.class.getClassLoader();
			if (classLoader != null) {
				getLog().debug("Trying to find [" + resource + "] using " + classLoader
						+ " class loader.");
				url = classLoader.getResource(resource);
				if (url != null) {
					return url;
				}
			}
		} catch (IllegalAccessException t) {
			getLog().warn(TSTR, t);
		} catch (InvocationTargetException t) {
			if (t.getTargetException() instanceof InterruptedException
					|| t.getTargetException() instanceof InterruptedIOException) {
				Thread.currentThread().interrupt();
			}
			getLog().warn(TSTR, t);
		} catch (Throwable t) {
			//
			// can't be InterruptedException or InterruptedIOException
			// since not declared, must be error or RuntimeError.
			getLog().warn(TSTR, t);
		}

		// Last ditch attempt: get the resource from the class path. It
		// may be the case that class was loaded by the Extension class
		// loader which the parent of the system class loader. Hence the
		// code below.
		getLog().debug("Trying to find [" + resource + "] using ClassLoader.getSystemResource().");
		return ClassLoader.getSystemResource(resource);
	}

	/**
	 * Removes any line breaks and trims the file name.
	 * @param resource
	 * @return
	 */
	private String cleanup(String fileName) {
		if(fileName == null || fileName.isEmpty())
			return null;
		return fileName.trim().replace("\n", "");
	}

	/**
	 * Get the Thread Context Loader which is a JDK 1.2 feature. If we are
	 * running under JDK 1.1 or anything else goes wrong the method returns
	 * <code>null<code>.
	 *
	 */
	private static ClassLoader getTCL() throws IllegalAccessException, InvocationTargetException {

		// Are we running on a JDK 1.2 or later system?
		Method method = null;
		try {
			method = Thread.class.getMethod("getContextClassLoader", null);
		} catch (NoSuchMethodException e) {
			// We are running on JDK 1.1
			return null;
		}

		return (ClassLoader) method.invoke(Thread.currentThread(), null);
	}

}
