With this plugin you will be able to test all your classes as part of the standard Maven testing cycle.

First, you need to list all your test files in a file called tester4j.xml:

	<tests>
		<file>TestFileA.json</file>
		<file>TestFileB.json</file>
	</tests>
	
You should place your tester4j.xml file in a folder relative to your project's classpath, 
for example in your src/main/resources folder (if you follow the standard Maven folder structure).

Place all your JSON test files in this same folder, or any other folder relative to your classpath. 
You can place them next to your java files too, just make sure to type the complete files path like this:

	<tests>
		<file>com/tester4j/Tester.json</file>
		<file>com/tester4j/demo/Examples.json</file>
	</tests>

Finally you should include the tester4j-maven-plugin in your pom.xml file as follows:

	<build>
		<plugins>
			...
			<plugin>
				<groupId>com.tester4j</groupId>
				<artifactId>tester4j-maven-plugin</artifactId>
				<version>0.1-SNAPSHOT</version>
				<executions>
					<execution>
						<phase>test</phase>
						<goals>
							<goal>runtests</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			...
		</plugins>
	</build>
	
Now, you can run all your tests with:

	mvn test

All test results will appear in the console:

	[INFO] --- tester4j-maven-plugin:0.1-SNAPSHOT:runtests (default) @ com.tester4j ---
	[INFO] === Executing Tester4j ===
	[INFO] Loading configuration file
	[INFO] Running test file: com.tester4j.Tester.json
	2017-08-02 21:29:15 INFO  Tester:220 - Pass: isAPrimitive([jhjb]) returns true
	2017-08-02 21:29:15 INFO  Tester:220 - Pass: isAPrimitive([123.45]) returns true
	2017-08-02 21:29:15 INFO  Tester:220 - Pass: isAPrimitive([false]) returns true
	2017-08-02 21:29:15 INFO  Tester:220 - Pass: isAPrimitive([{name=John}]) returns false
	2017-08-02 21:29:15 INFO  Tester:220 - Pass: isAPrimitive([null]) returns true
	2017-08-02 21:29:15 INFO  Tester:77 - -- All tests passed --
	[INFO] Running test file: com.tester4j.demo.Examples.json
	2017-08-02 21:29:15 INFO  Tester:220 - Pass: sum([12.0, 14.0]) returns 26.0
	2017-08-02 21:29:15 INFO  Tester:220 - Pass: sum([14.78, 56.0001]) returns 70.7801
	
